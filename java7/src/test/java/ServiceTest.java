import org.junit.Assert;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertArrayEquals;


public class ServiceTest {

    @Test
    public void BinaryStreamTest() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, 6};

        try( DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")))) {
            Service.burnBinaryStreamArray(out, array);
        }catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream("test.txt")));
        int [] array1 = Service.readBinaryStreamArray(in, array);
        assertArrayEquals(array, array1);


    }

    @Test
    public void CharacterStreamTest() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, 6};
        try(BufferedWriter out = new BufferedWriter(new FileWriter("test.txt"))){
            Service.writeArrayCharacterStream(out, array);
        }
        catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
        BufferedReader in = new BufferedReader(new FileReader("test.txt"));
        int [] array1 = Service.readArrayCharacterStream(in, array);
        assertArrayEquals(array, array1);
    }



    @Test
    public void ArrayFromGivenPositionTest() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        try(DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")))){
            Service.burnBinaryStreamArray(out, array);
        } catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        int [] array1=new int[3];
        int [] array2 = new int [] { 4, 55, -66};
        assertArrayEquals(array2, Service.readArrayFromGivenPosition(f, array1,3));
    }

    @Test
    public void ArrayFromGivenPositionTest1() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        try(DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")))){
        Service.burnBinaryStreamArray(out, array);
        } catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        int [] array1=new int[]{};
        int [] array2 = new int [] {};
        assertArrayEquals(array2,Service.readArrayFromGivenPosition(f, array1,6));
    }

    @Test
    public void ArrayFromGivenPositionTest2() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        try(DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")))){
        Service.burnBinaryStreamArray(out, array);
        }catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        int [] array1=new int[array.length];
        try {
             Service.readArrayFromGivenPosition(f, array1,7);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }


    /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
java.lang.IllegalArgumentException: File does not exit

	at Service.getListOfAllFiles(Service.java:75)
	at ServiceTest.ListOfAllFilesTest(ServiceTest.java:78)
    */
    @Test
    public void ListOfAllFilesTest() throws IOException {
        FileFilterExtends filter2 = new FileFilterExtends(".docx");
        File file2 = new File("TestNew");
        String[] arrayFilter1 =  Service.getListOfAllFiles(file2,".docx");
        String[] arrayFilter = new String[] {"0.docx", "4.docx"};
       assertArrayEquals(arrayFilter, arrayFilter1);
    }

    /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
     Не работает! Пишет

    java.lang.IllegalArgumentException: File does not exit

	at Service.getListOfAllFiles(Service.java:75)
	at ServiceTest.ListOfAllFilesTest1(ServiceTest.java:87)
    */
    @Test
    public void ListOfAllFilesTest1() throws IOException {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("TestNew");
        String[] arrayFilter1 =  Service.getListOfAllFiles(file2, ".txt");
        String[] arrayFilter = new String[] {"1.txt", "2.txt", "3.txt"};
        assertArrayEquals(arrayFilter, arrayFilter1);
    }

    @Test
    public void ListOfAllFilesTest2() throws IOException {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("Test.txt");
        try {
            String[] arrayFilter1 =  Service.getListOfAllFiles(file2,".txt" );
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }

    @Test
    public void ListOfAllFilesTest3() throws IOException {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("Papka");
        try {
            String[] arrayFilter1 =  Service.getListOfAllFiles(file2, ".txt");
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }



}