import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ServiceHouseTest {

    @Test
    public void readAndWriteTest() throws IOException, ClassNotFoundException {
       // ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("Test.txt")));
        Person p1 = new Person("Иван", "Иванов", "Иванович", "04.05.1983");
        Person p2 = new Person("Степан", "Иванов", "Иванович", "04.05.1973");
        Person p3 = new Person("Василий", "Петров", "Иванович", "04.05.1989");
        Person p4 = new Person("Людмила", "Петрова", "Ивановна", "04.05.1992");
        Person p5 = new Person("Игорь", "Семенов", "Иванович", "04.05.1978");

        ArrayList<Person> personList = new ArrayList<Person>();
        personList.add(p1);
        personList.add(p2);

        ArrayList<Person> personList1 = new ArrayList<Person>();
        personList1.add(p3);
        personList1.add(p4);

        ArrayList<Person> personList2 = new ArrayList<Person>();
        personList2.add(p5);


        ArrayList<Flat> fList = new ArrayList<Flat>();
        Flat f1 = new Flat(1, 50.9, personList);
        Flat f2 = new Flat(2, 60.9, personList1);
        Flat f3 = new Flat(3, 40.9, personList2);
        fList.add(f1);
        fList.add(f2);
        fList.add(f3);

        House h = new House("123332212", "ul.Pushkina, 29",
                new Person("Иван", "Иванов", "Иванович", "04.05.1983"), fList);
        try (ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("Test.txt")));){
            ServiceHouse.writeHouse(out,h);
        }catch (IOException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }


        /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
         Не работает! Вы не закрываете файлы, и лечите проблему расставлением команды flush.
         Уберите flush из writeHouse. Окружите все файлы трай блоком с ресурсами.

         try( ObjectOutput out =  ...) {
            ServiceHouse.writeHouse(out,h);
         }
        */
        ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Test.txt")));
        House h1 = ServiceHouse.readHouse(in);
        assertEquals(h, h1);


    }

    @Test
    public void serializationAndDeserializationTest() throws IOException, ClassNotFoundException {
        Person p1 = new Person("Иван", "Иванов", "Иванович", "04.05.1983");
        Person p2 = new Person("Степан", "Иванов", "Иванович", "04.05.1973");
        Person p3 = new Person("Василий", "Петров", "Иванович", "04.05.1989");
        Person p4 = new Person("Людмила", "Петрова", "Ивановна", "04.05.1992");
        Person p5 = new Person("Игорь", "Семенов", "Иванович", "04.05.1978");

        ArrayList<Person> personList = new ArrayList<Person>();
        personList.add(p1);
        personList.add(p2);

        ArrayList<Person> personList1 = new ArrayList<Person>();
        personList1.add(p3);
        personList1.add(p4);

        ArrayList<Person> personList2 = new ArrayList<Person>();
        personList2.add(p5);


        ArrayList<Flat> fList = new ArrayList<>();
        Flat f1 = new Flat(1, 50.9, personList);
        Flat f2 = new Flat(2, 60.9, personList1);
        Flat f3 = new Flat(3, 40.9, personList2);
        fList.add(f1);
        fList.add(f2);
        fList.add(f3);

        House h = new House("123332212", "ul.Pushkina, 29",
                new Person("Иван", "Иванов", "Иванович", "04.05.1983"), fList);

        String out = ServiceHouse.serialization(h);
        House h1 = ServiceHouse.deserialization(out);

        assertTrue(h.equals( h1));

    }
}