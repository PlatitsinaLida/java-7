import java.io.*;
import java.util.Scanner;

public class Service {

    //Записать массив целых чисел в двоичный поток.
    public static void burnBinaryStreamArray(DataOutputStream out, int[] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            out.writeInt(array[i]);
        }
        out.close();
    }

    //Прочитать массив целых чисел из двоичного потока. Предполагается,
    //что до чтения массив уже создан, нужно прочитать n чисел, где n — длина массива.
    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Массив уже создан! Не нужно его создавать - он передается в функцию.
    */
    public static int[] readBinaryStreamArray(DataInputStream in, int[] arrayAnswer) throws IOException {

        for (int i = 0; i < arrayAnswer.length; i++) {
            arrayAnswer[i] = in.readInt();
        }
        return arrayAnswer;
    }

    //Аналогично, используя символьные потоки. В потоке числа должны разделяться пробелами.
    public static void writeArrayCharacterStream(Writer out, int[] array) throws IOException {
        PrintWriter prWr = new PrintWriter(out);
        for (int i = 0; i < array.length; i++) {
            prWr.print(array[i]);
            prWr.print(' ');
        }
        out.close();
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Массив уже создан! Не нужно его создавать - он передается в функцию.
    */
    public static int[] readArrayCharacterStream(Reader in, int[] arrayAnswer) throws IOException {

        Scanner scanner = new Scanner(in);
        for (int i = 0; i < arrayAnswer.length; i++) {
            arrayAnswer[i] = scanner.nextInt();
        }
        return arrayAnswer;
    }

    //Используя класс RandomAccessFile, прочитайте массив целых чисел, начиная с заданной позиции.
    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Массив уже создан! Не нужно его создавать - он передается в функцию.
     Не нужно закрывать в функции файл, который она не открывала.
    */
    public static int[] readArrayFromGivenPosition(RandomAccessFile rAF, int[] s,int pointer) throws IOException {
        try {
            rAF.seek(pointer * 4);
        } catch (IOException e) {
            System.err.println("Pointer error" + e);
        }
        if (pointer <= rAF.length() / 4) {
            int len=(int) (rAF.length() - rAF.getFilePointer()) / 4;
         //   s=new int[(int) (rAF.length() - rAF.getFilePointer()) / 4];
            for (int i = 0; i < len; i++) {
                s[i]=rAF.readInt();
            }

            return s;
        } else {
            throw new IllegalArgumentException("Pointer went beyond");
        }

    }

    //Используя класс File, получите список всех файлов с заданным расширением
    // в заданном каталоге(поиск в подкаталогах выполнять не надо).
    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Расширение передается в функцию как строка! Зачем вам менять право доступа на запись к папке?
    */
    public static String[] getListOfAllFiles(File file, String exten) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                file.setWritable(false);

                String[] files = file.list(new FileFilterExtends(exten));
                return files;
            } else {
                throw new IllegalArgumentException("File is not directory");
            }
        } else {
            throw new IllegalArgumentException("File does not exit");
        }

    }


}
