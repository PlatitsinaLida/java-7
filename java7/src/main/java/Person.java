import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Objects;

//написать класс Person (человек), содержит Ф.И.О. (раздельно), дату рождения,
//В этих классах предусмотреть нужные конструкторы, геттеры, equals и hashCode.
public class Person implements Serializable {
    private String lastName;//фамилия
    private String firstName;//имя
    private String middleName;//отчество
    private String dateOfBirth;

    public void serialize(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public Person() {
        super();

    }

    public Person(String lastName, String firstName, String middleName, String dateOfBirth) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(lastName, person.lastName) &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(middleName, person.middleName) &&
                Objects.equals(dateOfBirth, person.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName,firstName,middleName,dateOfBirth);
    }
}
