import java.io.File;
import java.io.FilenameFilter;

public class FileFilterExtends implements FilenameFilter {
    private String extension;

    public FileFilterExtends (String string) {
        this.extension = string;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(extension);
    }
}

