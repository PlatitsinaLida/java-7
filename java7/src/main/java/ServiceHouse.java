
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;


//Напишите сервисный класс с методами, которые сериализуют и
// десериализуют объект типа House в заданный поток средствами Java.
public class ServiceHouse {
    public static void writeHouse(ObjectOutput out, House house) throws IOException {
        house.serialize(out);
        out.close();
    }

    public static House readHouse (ObjectInput in) throws IOException, ClassNotFoundException {
        return (House) in.readObject();
    }

    public static String serialization(House house) throws IOException {
        ObjectMapper mapper= new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    public static House deserialization(String jsonString) throws IOException, JsonMappingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, House.class);
    }
}