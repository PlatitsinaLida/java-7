import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

//написать класс Flat (квартира), содержит номер, площадь, данные о владельцах (список людей),
//В этих классах предусмотреть нужные конструкторы, геттеры, equals и hashCode.
public class Flat implements Serializable {
   private int number;
   private double area;
   private ArrayList<Person> owners;

    public Flat() {
        super();
    }

    public void serialize(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public Flat(int number, double area, ArrayList<Person> owners) {
        this.number = number;
        this.area = area;
        this.owners = owners;
    }

    public int getNumber() {
        return number;
    }

    public double getArea() {
        return area;
    }

    public ArrayList<Person> getOwners() {
        return owners;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setOwners(ArrayList<Person> owners) {
        this.owners = owners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        Flat flat = (Flat) o;
        return getNumber() == flat.getNumber() &&
                Double.compare(flat.getArea(), getArea()) == 0 &&
                Objects.equals(getOwners(), flat.getOwners());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getArea(), getOwners());
    }
}

